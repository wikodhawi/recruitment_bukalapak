package com.dhaba.templatenewproject.api

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.dhaba.templatenewproject.data.api.ApiService
import com.dhaba.templatenewproject.data.model.contact.Result
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.buffer
import okio.source
import org.hamcrest.CoreMatchers
import org.junit.*
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@RunWith(JUnit4::class)
class ApiTest {
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var service: ApiService

    private lateinit var mockWebServer: MockWebServer

    @Before
    fun createService() {
        mockWebServer = MockWebServer()
        service = Retrofit.Builder()
            .baseUrl(mockWebServer.url(""))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiService::class.java)
    }

    @After
    fun stopService() {
        mockWebServer.shutdown()
    }

    @Test
    fun tesEndpoint() {
        runBlocking {
            enqueueResponse("random_user.json")
            val resultResponse = service.getContact()

            val request = mockWebServer.takeRequest(1, TimeUnit.SECONDS)
            Assert.assertNotNull(resultResponse)
            Assert.assertThat(request!!.path, CoreMatchers.`is`("/api?reults=5&exc=login,registered,id,nat,nat&nat=us&noinfo"))

//            enqueueResponse("github_user_all.json")
//            val resultResponseAll = service.listGithubUsers( 1, 25)
//
//            val requestAll = mockWebServer.takeRequest(1, TimeUnit.SECONDS)
//            Assert.assertNotNull(resultResponseAll)
//            Assert.assertThat(requestAll!!.path, CoreMatchers.`is`("/users?since=1&per_page=25"))
        }
    }

    @Test
    fun getGithubUserItem(){
        runBlocking {
            enqueueResponse("random_user.json")
            val resultResponse = service.getContact()
            val selectedFirstUser = resultResponse.results?.get(0)
//
            Assert.assertThat(selectedFirstUser?.gender, CoreMatchers.`is`("male"))
//            Assert.assertThat(selectedFirstItemGithubSearch.eventsUrl, CoreMatchers.`is`("https://api.github.com/users/p/events{/privacy}"))
//            Assert.assertThat(selectedFirstItemGithubSearch.followersUrl, CoreMatchers.`is`("https://api.github.com/users/p/followers"))
//            Assert.assertThat(selectedFirstItemGithubSearch.followingUrl, CoreMatchers.`is`("https://api.github.com/users/p/following{/other_user}"))
//            Assert.assertThat(selectedFirstItemGithubSearch.gistsUrl, CoreMatchers.`is`("https://api.github.com/users/p/gists{/gist_id}"))
//            Assert.assertThat(selectedFirstItemGithubSearch.gravatarId, CoreMatchers.`is`(""))
//            Assert.assertThat(selectedFirstItemGithubSearch.htmlUrl, CoreMatchers.`is`("https://github.com/p"))
//            Assert.assertThat(selectedFirstItemGithubSearch.id, CoreMatchers.`is`(125612))
//            Assert.assertThat(selectedFirstItemGithubSearch.login, CoreMatchers.`is`("p"))
//            Assert.assertThat(selectedFirstItemGithubSearch.nodeId, CoreMatchers.`is`("MDQ6VXNlcjEyNTYxMg=="))
//            Assert.assertThat(selectedFirstItemGithubSearch.organizationsUrl, CoreMatchers.`is`("https://api.github.com/users/p/orgs"))
//            Assert.assertThat(selectedFirstItemGithubSearch.receivedEventsUrl, CoreMatchers.`is`("https://api.github.com/users/p/received_events"))
//            Assert.assertThat(selectedFirstItemGithubSearch.reposUrl, CoreMatchers.`is`("https://api.github.com/users/p/repos"))
//            Assert.assertThat(selectedFirstItemGithubSearch.score, CoreMatchers.`is`(1.0))
//            Assert.assertThat(selectedFirstItemGithubSearch.siteAdmin, CoreMatchers.`is`(false))
//            Assert.assertThat(selectedFirstItemGithubSearch.starredUrl, CoreMatchers.`is`("https://api.github.com/users/p/starred{/owner}{/repo}"))
//            Assert.assertThat(selectedFirstItemGithubSearch.subscriptionsUrl, CoreMatchers.`is`("https://api.github.com/users/p/subscriptions"))
//            Assert.assertThat(selectedFirstItemGithubSearch.type, CoreMatchers.`is`("User"))
//            Assert.assertThat(selectedFirstItemGithubSearch.url, CoreMatchers.`is`("https://api.github.com/users/p"))
//
//            enqueueResponse("github_user_all.json")
//            val resultResponseAll = service.listGithubUsers( 1, 25)
//            val selectedFirstItemGithubAll = resultResponseAll[0]
//
//            Assert.assertThat(selectedFirstItemGithubAll.avatarUrl, CoreMatchers.`is`("https://avatars0.githubusercontent.com/u/2?v=4"))
//            Assert.assertThat(selectedFirstItemGithubAll.eventsUrl, CoreMatchers.`is`("https://api.github.com/users/defunkt/events{/privacy}"))
//            Assert.assertThat(selectedFirstItemGithubAll.followersUrl, CoreMatchers.`is`("https://api.github.com/users/defunkt/followers"))
//            Assert.assertThat(selectedFirstItemGithubAll.followingUrl, CoreMatchers.`is`("https://api.github.com/users/defunkt/following{/other_user}"))
//            Assert.assertThat(selectedFirstItemGithubAll.gistsUrl, CoreMatchers.`is`("https://api.github.com/users/defunkt/gists{/gist_id}"))
//            Assert.assertThat(selectedFirstItemGithubAll.gravatarId, CoreMatchers.`is`(""))
//            Assert.assertThat(selectedFirstItemGithubAll.htmlUrl, CoreMatchers.`is`("https://github.com/defunkt"))
//            Assert.assertThat(selectedFirstItemGithubAll.id, CoreMatchers.`is`(2))
//            Assert.assertThat(selectedFirstItemGithubAll.login, CoreMatchers.`is`("defunkt"))
//            Assert.assertThat(selectedFirstItemGithubAll.nodeId, CoreMatchers.`is`("MDQ6VXNlcjI="))
//            Assert.assertThat(selectedFirstItemGithubAll.organizationsUrl, CoreMatchers.`is`("https://api.github.com/users/defunkt/orgs"))
//            Assert.assertThat(selectedFirstItemGithubAll.receivedEventsUrl, CoreMatchers.`is`("https://api.github.com/users/defunkt/received_events"))
//            Assert.assertThat(selectedFirstItemGithubAll.reposUrl, CoreMatchers.`is`("https://api.github.com/users/defunkt/repos"))
//            Assert.assertThat(selectedFirstItemGithubAll.siteAdmin, CoreMatchers.`is`(false))
//            Assert.assertThat(selectedFirstItemGithubAll.starredUrl, CoreMatchers.`is`("https://api.github.com/users/defunkt/starred{/owner}{/repo}"))
//            Assert.assertThat(selectedFirstItemGithubAll.subscriptionsUrl, CoreMatchers.`is`("https://api.github.com/users/defunkt/subscriptions"))
//            Assert.assertThat(selectedFirstItemGithubAll.type, CoreMatchers.`is`("User"))
//            Assert.assertThat(selectedFirstItemGithubAll.url, CoreMatchers.`is`("https://api.github.com/users/defunkt"))
        }
    }

    @Test
    fun testTotalReturnData() {
        runBlocking {
//            enqueueResponse("github_user_search.json")
//            val resultResponse = service.searchGithubUsers("p", 1, 25)
//            Assert.assertThat(resultResponse.items.size, CoreMatchers.`is`(25))
//
//            enqueueResponse("github_user_all.json")
//            val resultResponseAll = service.listGithubUsers( 1, 25)
//            Assert.assertThat(resultResponseAll.size, CoreMatchers.`is`(25))
        }
    }

    private fun enqueueResponse(fileName: String, headers: Map<String, String> = emptyMap()) {
        val inputStream = javaClass.classLoader!!
            .getResourceAsStream("api-response/$fileName")
        val source = inputStream.source().buffer()
        val mockResponse = MockResponse()
        for ((key, value) in headers) {
            mockResponse.addHeader(key, value)
        }
        mockWebServer.enqueue(mockResponse.setBody(
            source.readString(Charsets.UTF_8))
        )
    }
}