package com.dhaba.templatenewproject.flow

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.dhaba.templatenewproject.data.api.ApiService
import com.dhaba.templatenewproject.data.model.QueryParameterData
import com.dhaba.templatenewproject.data.model.githubusers.GithubUser
import kotlinx.coroutines.flow.Flow

class GithubUsersFlowRepositoryImpl: GithubUsersFlowRepository {

    override fun getUsers(apiService: ApiService, queryParameterData: QueryParameterData): Flow<PagingData<GithubUser>> {
        val pagingSource = GithubUsersFlowPagingSource(apiService, queryParameterData)
        return Pager(
            config = PagingConfig(
                pageSize = 20,
                enablePlaceholders = true,
                prefetchDistance = 5,
                initialLoadSize = 40),
            pagingSourceFactory = { pagingSource }
        ).flow
    }
}