package com.dhaba.templatenewproject.flow

import android.accounts.NetworkErrorException
import androidx.paging.PagingSource
import com.dhaba.templatenewproject.data.api.ApiService
import com.dhaba.templatenewproject.data.model.QueryParameterData
import com.dhaba.templatenewproject.data.model.githubuserall.GithubUserAllItem
import com.dhaba.templatenewproject.data.model.githubusers.GithubUser
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

class GithubUsersFlowPagingSource (
    private val api: ApiService,
    private val queryParameterData: QueryParameterData
) : PagingSource<Int, GithubUser>() {
    private var page = 1
    private val perPage = 25

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, GithubUser> {
        val position = params.key ?: 1

        var dataList = mutableListOf<GithubUser>()
        return try {
            if(queryParameterData.query.isBlank())
            {
                try {
                    val response = getUserAll(position, perPage)
                    for(i in response)
                    {
                        dataList.add(i.changeGithubUserAllToGithubUser())
                    }
                    LoadResult.Page(
                        data = dataList,
                        prevKey = if (position == 1 || dataList.size < perPage) null else position - 1,
                        nextKey = if (position == response.size) null else position + 1
                    )
                } catch (e: java.lang.Exception) {
                    LoadResult.Error(e)
                }
            }
            else
            {
                try {
                    val response = getUser(queryParameterData.query, position, perPage)
                    dataList = response.items.toMutableList()
                    if(dataList.size>0)
                    {
                        LoadResult.Page(
                            data = dataList,
                            prevKey = if (position == 1 ) null else position - 1,
                            nextKey = if (position == response.totalCount/(response.items.size*position)+1) null else position + 1
                        )
                    }
                    else
                        LoadResult.Page(
                            data = mutableListOf(),
                            prevKey = null,
                            nextKey = null
                        )
                } catch (e: java.lang.Exception) {
                    LoadResult.Error(NetworkErrorException())
                }
            }

//            service.get(
//                apiKey = apiKey,
//                language = locale.language,
//                page = position
//            ).run {
//                val data = mapper.transform(this, locale)
//
//                LoadResult.Page(
//                    data = data.movies,
//                    prevKey = if (position == 1) null else position - 1,
//                    nextKey = if (position == this.total) null else position + 1
//                )
//            }
        } catch (e: Exception) {
            return LoadResult.Error(e)
        }
    }

    private suspend fun getUser(query: String, page: Int, perPage : Int) = api.searchGithubUsers(query, page, perPage)
    private suspend fun getUserAll(since: Int, perPage : Int) = api.listGithubUsers(since, perPage)

    private fun GithubUserAllItem.changeGithubUserAllToGithubUser() : GithubUser =
        GithubUser(
            avatarUrl = this.avatarUrl,
            eventsUrl = this.eventsUrl,
            followersUrl = this.followersUrl,
            followingUrl = this.followingUrl,
            gistsUrl = this.gistsUrl,
            gravatarId = this.gravatarId,
            htmlUrl = this.htmlUrl,
            id = this.id,
            login = this.login,
            nodeId = this.nodeId,
            organizationsUrl = this.organizationsUrl,
            receivedEventsUrl = this.receivedEventsUrl,
            reposUrl = this.reposUrl,
            score = 0.0,
            siteAdmin = this.siteAdmin,
            starredUrl = this.starredUrl,
            subscriptionsUrl = this.subscriptionsUrl,
            type = this.type,
            url = this.url
        )
}