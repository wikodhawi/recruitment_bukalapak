package com.dhaba.templatenewproject.flow

import androidx.paging.PagingData
import com.dhaba.templatenewproject.data.api.ApiService
import com.dhaba.templatenewproject.data.model.QueryParameterData
import com.dhaba.templatenewproject.data.model.githubusers.GithubUser
import kotlinx.coroutines.flow.Flow

interface GithubUsersFlowRepository {
    fun getUsers(apiService: ApiService, queryParameterData: QueryParameterData): Flow<PagingData<GithubUser>>
}