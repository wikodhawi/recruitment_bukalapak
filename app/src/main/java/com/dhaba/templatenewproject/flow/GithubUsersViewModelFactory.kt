package com.dhaba.templatenewproject.flow

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dhaba.templatenewproject.viewmodel.githubusers.GithubUsersViewModel

class GithubUsersViewModelFactory (private val repository: GithubUsersFlowRepository): ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(GithubUsersViewModel::class.java)) {
            return GithubUsersViewModel(
                repository
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}