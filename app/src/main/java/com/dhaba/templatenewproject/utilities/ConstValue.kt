package com.dhaba.templatenewproject.utilities

object ConstValue {
    const val IS_DARK_MODE_DISABLE = 2
    const val IS_DARK_MODE_ENABLE = 1
    const val IS_DARK_MODE_NULL = 0
    const val FONT_SIZE_SETTING_POSITION: Int = 0
    const val ALPHA_DISABLE: Float = 0.3F
    const val ALPHA_ENABLE: Float = 1F
    const val LANGUAGE: Long =2L
    const val BACKUP_RESTORE_SETTING: Long =3L
    const val DARK_MODE_SETTING: Long =4L
    const val PIN: Long = 1L
    const val FONT_SIZE: Long = 0L
    const val LENGTH_PREVIEW_NOTE: Int = 40
    const val DATE_FORMAT_DETAIL_NOTE: String = "d MMMM HH:mm"
    val IN: String = "in"
    const val FARMER = "PTN"
    const val SUPPLIER = "PKS"

    val sharedPreferencesName = "DailyNotepadSharedPref"
    val notificationKoperasi = 2
    val notificationPetani = 3
    const val CURRENCY = "Rp"
    val JOB_VACANCY_SHOW_TIME = /*"dd MMM yyyy '-' hh:mm a"*/ "EEEE, d MMM yyyy HH:mm Z"
    val PARSE_DATE_LARAVEL = "yyyy-MM-dd hh:mm:ss"

}