package com.dhaba.templatenewproject.utilities

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import com.dhaba.templatenewproject.data.api.ApiService
import com.dhaba.templatenewproject.data.model.QueryParameterData
import com.dhaba.templatenewproject.flow.GithubUsersFlowRepositoryImpl
import com.dhaba.templatenewproject.flow.GithubUsersViewModelFactory

object Injection {
    fun provideFlowViewModel(): ViewModelProvider.Factory {
        val repository =
            GithubUsersFlowRepositoryImpl()

        return GithubUsersViewModelFactory(
            repository
        )
    }
}