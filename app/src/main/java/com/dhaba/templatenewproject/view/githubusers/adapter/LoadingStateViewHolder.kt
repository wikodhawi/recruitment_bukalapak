package com.dhaba.templatenewproject.view.githubusers.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.dhaba.templatenewproject.R
import com.dhaba.templatenewproject.databinding.ItemUserGithubLoadingShimmerBinding

class LoadingStateViewHolder(
    private val binding: ItemUserGithubLoadingShimmerBinding
): RecyclerView.ViewHolder(binding.root) {

    fun bind(loadState: LoadState, mAdapter: GithubUserAdapter) {
        binding.root.isVisible = (loadState is LoadState.Loading || loadState is LoadState.Error)
        binding.root.setOnClickListener {
            mAdapter.retry()
        }
    }

    companion object {
        fun create(parent: ViewGroup): LoadingStateViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_user_github_loading_shimmer, parent, false)

            val binding = ItemUserGithubLoadingShimmerBinding.bind(view)

            return LoadingStateViewHolder(
                binding
            )
        }
    }
}