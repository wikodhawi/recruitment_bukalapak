package com.dhaba.templatenewproject.view.selectcontact

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.dhaba.templatenewproject.R
import com.dhaba.templatenewproject.base.BaseActivity
import com.dhaba.templatenewproject.data.api.ApiResult
import com.dhaba.templatenewproject.data.model.contact.Result
import com.dhaba.templatenewproject.databinding.ActivityMainBinding
import com.dhaba.templatenewproject.databinding.ActivitySelectContactBinding
import com.dhaba.templatenewproject.view.selectcontact.adapter.SelectContactAdapter
import com.dhaba.templatenewproject.viewmodel.selectcontact.SelectContactViewModel
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SelectContactActivity : BaseActivity() {
    private lateinit var binding: ActivitySelectContactBinding
    private val viewModel: SelectContactViewModel by viewModels()
    private lateinit var adapter: SelectContactAdapter

    companion object {
        const val REQUEST_CONTACT = 100
        const val CONTACT_SELECTED = "contactSelected"
    }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_select_contact)
        observeViewModel()
        setupToolbar()
        viewModel.getContactPhone()

        binding.rcySelectContact.layoutManager = LinearLayoutManager(applicationContext)
        val dividerItemDecoration = DividerItemDecoration(
            binding.rcySelectContact.context,
            DividerItemDecoration.VERTICAL
        )
        binding.rcySelectContact.addItemDecoration(dividerItemDecoration)
    }

    private fun observeViewModel() {
        val dialog = ProgressDialog(this)
        viewModel.contact.observe(this, {
            if(it.status == ApiResult.Status.LOADING) {
                dialog.show()
            }
            else if(it.status == ApiResult.Status.ERROR)
            {
                dialog.dismiss()
                Toast.makeText(applicationContext, it.message, Toast.LENGTH_SHORT).show()
            }
            else if(it.status == ApiResult.Status.SUCCESS) {
                dialog.dismiss()
                adapter = SelectContactAdapter(it.data?.results?: listOf(), object : SelectContactAdapter.SelectInterface{
                    override fun onItemClick(result: Result) {
                        val intent = intent
                        intent.putExtra(CONTACT_SELECTED, Gson().toJson(result))
                        setResult(RESULT_OK, intent)
                        finish()
                    }
                })
                binding.rcySelectContact.adapter = adapter
            }
        })
    }

    private fun setupToolbar()
    {
        binding.toolbar.lblTitleToolbarHome.text = getString(R.string.pilih_kontak)
        binding.toolbar.imgClose.visibility = View.VISIBLE
        binding.toolbar.imgClose.setOnClickListener {
            finish()
        }
    }
}