package com.dhaba.templatenewproject.view.githubusers.adapter

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.dhaba.templatenewproject.R
import com.dhaba.templatenewproject.data.api.UiModel

class GithubUserAdapter : PagingDataAdapter<UiModel.GithubUserItem, RecyclerView.ViewHolder>(COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return GithubUserViewHolder.create(parent)
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_user_github
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getItem(position)?.let {
            when (it) {
                is UiModel.GithubUserItem -> (holder as GithubUserViewHolder).bind(it.githubUser, position)
            }
        }
    }

    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<UiModel.GithubUserItem>() {
            override fun areItemsTheSame(oldItem: UiModel.GithubUserItem, newItem: UiModel.GithubUserItem): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: UiModel.GithubUserItem, newItem: UiModel.GithubUserItem): Boolean {
                return oldItem == newItem
            }
        }
    }

}
