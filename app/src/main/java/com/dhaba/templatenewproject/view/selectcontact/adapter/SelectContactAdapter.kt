package com.dhaba.templatenewproject.view.selectcontact.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dhaba.templatenewproject.data.model.contact.Result
import com.dhaba.templatenewproject.databinding.ItemContactBinding

class SelectContactAdapter(private val contacts: List<Result>, private val selectInterface: SelectInterface) : RecyclerView.Adapter<SelectContactAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val binding = ItemContactBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemViewHolder(binding)
    }

    class ItemViewHolder(val binding: ItemContactBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = contacts[position]
        holder.binding.lblContactName.text = "${item.name?.first} ${item.name?.last}"
        Glide.with(holder.binding.root.context).load(item.picture?.thumbnail).into(holder.binding.imgContact)
        holder.binding.root.setOnClickListener {
            selectInterface.onItemClick(item)
        }
    }

    override fun getItemCount(): Int {
        return contacts.size
    }

    interface SelectInterface {
        fun onItemClick(result: Result)
    }
}