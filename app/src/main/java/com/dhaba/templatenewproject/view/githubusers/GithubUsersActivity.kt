package com.dhaba.templatenewproject.view.githubusers

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dhaba.templatenewproject.R
import com.dhaba.templatenewproject.base.BaseActivity
import com.dhaba.templatenewproject.data.api.ApiService
import com.dhaba.templatenewproject.data.model.QueryParameterData
import com.dhaba.templatenewproject.databinding.ActivityGithubUsersBinding
import com.dhaba.templatenewproject.utilities.CustomDialog
import com.dhaba.templatenewproject.utilities.Injection
import com.dhaba.templatenewproject.view.githubusers.adapter.GithubUserAdapter
import com.dhaba.templatenewproject.view.githubusers.adapter.LoadingStateAdapter
import com.dhaba.templatenewproject.viewmodel.githubusers.GithubUsersViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class GithubUsersActivity : BaseActivity() {
    private lateinit var binding: ActivityGithubUsersBinding
    private lateinit var viewModel:GithubUsersViewModel
    private lateinit var mAdapter: GithubUserAdapter
    private lateinit var dialog: Dialog

    @Inject
    lateinit var apiService: ApiService


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_github_users)
//        loadDataGithubUsers("")
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(applicationContext)
        binding.rcyUserGithub.layoutManager = mLayoutManager
        binding.rcyUserGithub.itemAnimator = DefaultItemAnimator()
        binding.lytLoadingUser.root.setOnClickListener {
            search()
        }

        binding.edtUserGithub.setOnTouchListener(View.OnTouchListener { v, event ->
            val drawableRight = 2
            when (event?.action) {
                MotionEvent.ACTION_UP ->
                    if(event.rawX >= (binding.edtUserGithub.right - binding.edtUserGithub.compoundDrawables[drawableRight].bounds.width())) {
                        search()
                        return@OnTouchListener true
                    }
            }

            v?.onTouchEvent(event) ?: true
        })

        binding.edtUserGithub.setOnEditorActionListener { _, actionId, _ ->
            if(actionId == EditorInfo.IME_ACTION_SEARCH)
            {
                search()
                val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(binding.edtUserGithub.windowToken, 0)
            }
            false
        }
        binding.swpMain.setOnRefreshListener {
            search()
            binding.swpMain.isRefreshing = false
        }
        search()
    }

    private fun search() {
        val body = QueryParameterData()
//        body.promotoR_ID = DataHolder.userlogin.userid
        body.query = binding.edtUserGithub.text.toString()
        viewModel = ViewModelProvider(this, Injection.provideFlowViewModel()).get(
                GithubUsersViewModel::class.java)

        mAdapter = GithubUserAdapter()

        binding.rcyUserGithub.layoutManager = LinearLayoutManager(this)
        binding.rcyUserGithub.adapter = mAdapter.withLoadStateHeaderAndFooter(
            header = LoadingStateAdapter(mAdapter),
            footer = LoadingStateAdapter(mAdapter)
        )

        mAdapter.addLoadStateListener { loadState ->
            val errorState = loadState.source.append as? LoadState.Error
                    ?: loadState.source.prepend as? LoadState.Error
                    ?: loadState.append as? LoadState.Error
                    ?: loadState.prepend as? LoadState.Error
                    ?: loadState.refresh as? LoadState.Error

            errorState?.let {
//                AlertDialog.Builder(this@GithubUsersActivity)
//                        .setTitle("Error")
//                        .setMessage(it.error.localizedMessage)
//                        .setNegativeButton("Cancel") { dialog, _ ->
//                            dialog.dismiss()
//                        }
//                        .setPositiveButton("Retry") { _, _ ->
//                            mAdapter.retry()
//                        }
//                        .show()
                if(!this::dialog.isInitialized || !dialog.isShowing)
                {
                    dialog = CustomDialog.createDialogWithTwoButton(this,
                        cancelable = false,
                        message = it.error.localizedMessage,
                        labelPositiveButton = getString(R.string.retry),
                        labelNegativeButton = getString(R.string.settings),
                        positiveAction = {
                            mAdapter.retry()
                        },
                        negativeAction = {
                            startActivityForResult(Intent(Settings.ACTION_SETTINGS), 0)
                        },
                        closeAction = {
                            Toast.makeText(applicationContext, getString(R.string.click_loading_view_to_retry), Toast.LENGTH_LONG).show()
                        })
                    dialog.show()
                }
                binding.lytLoadingUser.root.visibility = View.VISIBLE
            }
        }

        lifecycleScope.launch {
            viewModel.getGithubUsers(apiService, body).collectLatest {
                if(it!= null) {
                    mAdapter.submitData(it)
                    binding.lytLoadingUser.root.visibility = View.GONE
                }

//                lodd.HideProgressDialog()
            }
        }

    }
}