package com.dhaba.templatenewproject.view.githubusers.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dhaba.templatenewproject.R
import com.dhaba.templatenewproject.data.model.githubusers.GithubUser
import com.dhaba.templatenewproject.databinding.ItemUserGithubBinding

class GithubUserViewHolder (private val binding: ItemUserGithubBinding)  : RecyclerView.ViewHolder(binding.root)  {
    fun bind(item: GithubUser, position: Int) {
        with(item) {
            Glide.with(itemView).load(item.avatarUrl).into(binding.imgUserGithub)
            binding.lblUsernameGithub.text = item.login
        }
    }

    companion object {
        fun create(parent: ViewGroup): GithubUserViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_user_github, parent, false)

            val binding = ItemUserGithubBinding.bind(view)

            return GithubUserViewHolder(
                    binding
            )
        }
    }
}