package com.dhaba.templatenewproject.view.main

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.dhaba.templatenewproject.R
import com.dhaba.templatenewproject.base.BaseActivity
import com.dhaba.templatenewproject.data.model.contact.Result
import com.dhaba.templatenewproject.databinding.ActivityMainBinding
import com.dhaba.templatenewproject.view.githubusers.GithubUsersActivity
import com.dhaba.templatenewproject.view.selectcontact.SelectContactActivity
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainActivity : BaseActivity() {

    private lateinit var binding: ActivityMainBinding
    private val contacts: HashMap<String, Result> = HashMap()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setupToolbar()
        binding.rcySelectedContact.layoutManager = LinearLayoutManager(applicationContext)
        val dividerItemDecoration = DividerItemDecoration(
            binding.rcySelectedContact.context,
            DividerItemDecoration.VERTICAL
        )
        binding.rcySelectedContact.addItemDecoration(dividerItemDecoration)
    }

    private fun setupToolbar()
    {
        binding.toolbar.lblTitleToolbarHome.text = getString(R.string.kelola_kontak)
        binding.toolbar.imgAdd.visibility = View.VISIBLE
        binding.toolbar.imgAdd.setOnClickListener {
            val intent = Intent(applicationContext, SelectContactActivity::class.java)
            startActivityForResult(intent, SelectContactActivity.REQUEST_CONTACT)
        }
        binding.toolbar.lblTitleToolbarHome.setOnClickListener {
            val intent = Intent(applicationContext, GithubUsersActivity::class.java)
            startActivity(intent)
        }
    }

    private fun setAdapter() {
        val listData = mutableListOf<Result>()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == SelectContactActivity.REQUEST_CONTACT && resultCode == RESULT_OK) {
            val contactsSelected = Gson().fromJson<Result>(data?.getStringExtra(SelectContactActivity.CONTACT_SELECTED), Result::class.java)
            contacts.put(contactsSelected?.phone?: "", contactsSelected)
            setAdapter()
        }
    }
}