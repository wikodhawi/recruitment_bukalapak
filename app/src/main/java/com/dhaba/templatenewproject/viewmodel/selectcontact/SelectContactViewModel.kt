package com.dhaba.templatenewproject.viewmodel.selectcontact

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dhaba.templatenewproject.data.api.ApiResult
import com.dhaba.templatenewproject.data.model.contact.ContactPhone
import com.dhaba.templatenewproject.data.repository.SelectContactRepository
import kotlinx.coroutines.launch

class SelectContactViewModel @ViewModelInject constructor(
    private val repository: SelectContactRepository
) : ViewModel() {
    private val _contact = MutableLiveData<ApiResult<ContactPhone>>()
    val contact : LiveData<ApiResult<ContactPhone>> get() = _contact

    fun getContactPhone() {
        viewModelScope.launch {
            _contact.postValue(ApiResult.loading())
            try {
                val contacts =repository.getContacts()
                _contact.postValue(ApiResult.success(contacts))
            }
            catch (e: Exception) {
                _contact.postValue(ApiResult.error(e.localizedMessage))
            }
//            Log.d("phone", noteEntity.toString())
        }
    }
}