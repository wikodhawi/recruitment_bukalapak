package com.dhaba.templatenewproject.viewmodel.githubusers

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.map
import com.dhaba.templatenewproject.data.api.ApiService
import com.dhaba.templatenewproject.data.api.UiModel
import com.dhaba.templatenewproject.data.model.QueryParameterData
import com.dhaba.templatenewproject.data.model.githubusers.GithubUser
import com.dhaba.templatenewproject.flow.GithubUsersFlowRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class GithubUsersViewModel (private val repository: GithubUsersFlowRepository) : ViewModel() {
    fun getGithubUsers(apiService: ApiService, queryParameterData: QueryParameterData): Flow<PagingData<UiModel.GithubUserItem>> {
        val result = repository.getUsers(apiService, queryParameterData = queryParameterData)
            .map { pagingData -> pagingData.map { UiModel.GithubUserItem(it) } }
            .cachedIn(viewModelScope)
        return result
    }
}