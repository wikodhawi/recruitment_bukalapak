package com.dhaba.templatenewproject.di.module

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.dhaba.templatenewproject.BuildConfig
import com.dhaba.templatenewproject.service.database.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import net.sqlcipher.database.SQLiteDatabase
import net.sqlcipher.database.SupportFactory
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class DatabaseModule {
    @Singleton
    @Provides
    fun provideSqlCipher(): SupportFactory {
        val passphrase: ByteArray = SQLiteDatabase.getBytes(DATABASE_PASSWORD.toCharArray())
        return SupportFactory(passphrase)
    }


    @Singleton
    @Provides
    fun provideAppDatabase(context: Context, supportFactory: SupportFactory): AppDatabase =
        Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME).allowMainThreadQueries().fallbackToDestructiveMigration().addCallback(object: RoomDatabase.Callback() {
        }).openHelperFactory(supportFactory)
            .build()

    @Singleton
    @Provides
    fun provideDummyEntityDao(appDatabase: AppDatabase) = appDatabase.dummyEntityDao()

    companion object {
        private const val DATABASE_NAME = BuildConfig.DATABASE_NAME
        private const val DATABASE_PASSWORD = BuildConfig.DATABASE_PASSWORD
    }
}
