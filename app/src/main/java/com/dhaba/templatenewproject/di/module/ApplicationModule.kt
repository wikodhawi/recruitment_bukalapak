package com.dhaba.templatenewproject.di.module

import android.app.Activity
import android.app.Application
import android.content.Context
import com.dhaba.templatenewproject.BuildConfig
import com.dhaba.templatenewproject.BuildConfig.BASE_URL2
import com.dhaba.templatenewproject.app.App
import com.dhaba.templatenewproject.base.BaseActivity
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.dhaba.templatenewproject.data.api.ApiHelper
import com.dhaba.templatenewproject.data.api.ApiHelperImpl
import com.dhaba.templatenewproject.data.api.ApiService
import com.dhaba.templatenewproject.utilities.ConstValue
import com.dhaba.templatenewproject.utilities.PrefHelper
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import javax.inject.Singleton
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(ApplicationComponent::class)
class ApplicationModule {

    @Provides
    fun provideBaseUrl() = BuildConfig.BASE_URL2

    @Provides
    @Singleton
    fun provideOkHttpClient() = if (BuildConfig.DEBUG) {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build()
    } else OkHttpClient
        .Builder()
        .build()

    @Singleton
    @Provides
    fun provideGson() = GsonBuilder().serializeNulls().create()

    @Provides
    @Singleton
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        BASE_URL2: String,
        gson: Gson
    ): Retrofit =
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(BASE_URL2)
            .client(okHttpClient)
            .build()

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)

    @Provides
    @Singleton
    fun provideApiHelper(apiHelper: ApiHelperImpl): ApiHelper = apiHelper

    @Singleton
    @Provides
    fun provideContext(application: Application): Context = application

    @Singleton
    @Provides
    fun provideMainApplication(application: Application): App = application as App

    @Provides
    fun prefHelper(context: Context) : PrefHelper {
        PrefHelper.setSharedPreferences(context, ConstValue.sharedPreferencesName, Context.MODE_PRIVATE)
        return PrefHelper()
    }

    @Provides
    fun provideBaseActivity(activity: Activity): BaseActivity {
        check(activity is BaseActivity) { "Every Activity is expected to extend BaseActivity" }
        return activity as BaseActivity
    }
}