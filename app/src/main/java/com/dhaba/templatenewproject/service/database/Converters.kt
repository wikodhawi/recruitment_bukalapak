package com.dhaba.templatenewproject.service.database

import androidx.room.TypeConverter
import com.dhaba.templatenewproject.utilities.extension.fromJson
import com.dhaba.templatenewproject.utilities.extension.toJson
import java.util.*
import kotlin.collections.ArrayList

class Converters {
    @TypeConverter
    fun fromList(value: String?): ArrayList<String>? = value?.let { value.fromJson() }

    @TypeConverter
    fun fromString(value: ArrayList<String>?): String? = value?.let { value.toJson() }

    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return if (date == null) null else date.getTime()
    }
}
