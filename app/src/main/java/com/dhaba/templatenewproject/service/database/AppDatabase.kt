package com.dhaba.templatenewproject.service.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.dhaba.templatenewproject.data.model.entity.DummyEntity
import com.dhaba.templatenewproject.service.database.dao.DummyEntityDao


@Database(
    entities =[DummyEntity::class], version = 1
)

@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun dummyEntityDao(): DummyEntityDao
}
