package com.dhaba.templatenewproject.data.model.contact


import com.google.gson.annotations.SerializedName

data class Timezone(
    @SerializedName("description")
    var description: String?,
    @SerializedName("offset")
    var offset: String?
)