package com.dhaba.templatenewproject.data.model.contact


import com.google.gson.annotations.SerializedName

data class ContactPhone(
    @SerializedName("results")
    var results: List<Result>?
)