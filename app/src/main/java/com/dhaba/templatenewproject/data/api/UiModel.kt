package com.dhaba.templatenewproject.data.api

import com.dhaba.templatenewproject.data.model.githubusers.GithubUser

sealed class UiModel {
    data class GithubUserItem(val githubUser: GithubUser) : UiModel()
}