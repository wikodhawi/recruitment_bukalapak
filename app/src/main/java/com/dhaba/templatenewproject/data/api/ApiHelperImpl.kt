package com.dhaba.templatenewproject.data.api

import com.dhaba.templatenewproject.data.model.contact.ContactPhone
import com.dhaba.templatenewproject.data.model.githubuserall.GithubUserAll
import com.dhaba.templatenewproject.data.model.githubusers.GithubUsers
import javax.inject.Inject

class ApiHelperImpl @Inject constructor(private val apiService: ApiService) : ApiHelper {

    override suspend fun getContacts(): ContactPhone = apiService.getContact()
    override suspend fun searchGithubUsers(query: String, page: Int, perPage: Int): GithubUsers = apiService.searchGithubUsers(query, page, perPage)
    override suspend fun listGithubUsers(since: Int, perPage: Int): GithubUserAll = apiService.listGithubUsers(since, perPage)
}