package com.dhaba.templatenewproject.data.repository

import com.dhaba.templatenewproject.data.api.ApiHelper
import javax.inject.Inject

class SelectContactRepository @Inject constructor(private val apiHelper: ApiHelper) {

    suspend fun getContacts() = apiHelper.getContacts()

}