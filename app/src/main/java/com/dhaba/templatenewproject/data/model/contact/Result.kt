package com.dhaba.templatenewproject.data.model.contact

import com.google.gson.annotations.SerializedName

data class Result(
    @SerializedName("cell")
    var cell: String?,
    @SerializedName("dob")
    var dob: Dob?,
    @SerializedName("email")
    var email: String?,
    @SerializedName("gender")
    var gender: String?,
    @SerializedName("location")
    var location: Location?,
    @SerializedName("name")
    var name: Name?,
    @SerializedName("phone")
    var phone: String?,
    @SerializedName("picture")
    var picture: Picture?
) {

}