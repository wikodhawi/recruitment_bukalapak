package com.dhaba.templatenewproject.data.api

import com.dhaba.templatenewproject.data.model.contact.ContactPhone
import com.dhaba.templatenewproject.data.model.githubuserall.GithubUserAll
import com.dhaba.templatenewproject.data.model.githubusers.GithubUsers
import retrofit2.http.Query

interface ApiHelper {

    suspend fun getContacts(): ContactPhone
    suspend fun searchGithubUsers(query: String, page: Int, perPage: Int): GithubUsers
    suspend fun listGithubUsers(since: Int, perPage: Int): GithubUserAll
}