package com.dhaba.templatenewproject.data.model.githubusers


import com.google.gson.annotations.SerializedName

data class GithubUsers(
    @SerializedName("incomplete_results")
    val incompleteResults: Boolean,
    @SerializedName("items")
    val items: List<GithubUser>,
    @SerializedName("total_count")
    val totalCount: Int
)