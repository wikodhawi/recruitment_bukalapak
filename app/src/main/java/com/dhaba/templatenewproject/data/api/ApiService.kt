package com.dhaba.templatenewproject.data.api

import com.dhaba.templatenewproject.data.model.contact.ContactPhone
import com.dhaba.templatenewproject.data.model.githubuserall.GithubUserAll
import com.dhaba.templatenewproject.data.model.githubusers.GithubUsers
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("api?reults=5&exc=login,registered,id,nat,nat&nat=us&noinfo")
    suspend fun getContact(): ContactPhone

    @GET("search/users")
    suspend fun searchGithubUsers(@Query("q") query: String,
                                  @Query("page") page: Int,
                                  @Query("per_page") perPage: Int): GithubUsers

    @GET("users")
    suspend fun listGithubUsers(@Query("since") since: Int,
                                @Query("per_page") perPage: Int): GithubUserAll
}