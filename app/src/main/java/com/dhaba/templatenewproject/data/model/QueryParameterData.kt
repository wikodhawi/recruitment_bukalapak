package com.dhaba.templatenewproject.data.model

data class QueryParameterData (
    var query: String = ""
)